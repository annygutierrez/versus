import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Dimensions
} from 'react-native';
import Carousel from 'react-native-looped-carousel';
import CarouselPageComponent from '../CarouselPage';
import TeamCardsListComponent from '../TeamCardsList';
import PlayersListComponent from '../PlayersList';

const { width, height } = Dimensions.get('window');

export default class SoccerTabComponent extends Component {
  state = {
    matchesCard: [
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Zeit Sports vs. La Máquina Telefónica - 8 vs. 8",
        place: "San Isidro",
        time: "5:50 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Barcelona vs. Claro - 10 vs. 10",
        place: "La Molina",
        time: "1:30 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "The 1975 vs. Barranquilla - 8 vs. 8",
        place: "Barranco",
        time: "3:00 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Entel vs. Sacramento - 11 vs. 11",
        place: "Cieneguilla",
        time: "2:00 PM"
      }
    ],
    players: [
      {
        name: "Paolo",
        image: require('../../../assets/user1.png')
      },
      {
        name: "Leopardi",
        image: require('../../../assets/user2.png')
      },
      {
        name: "Camilo",
        image: require('../../../assets/user3.png')
      },
      {
        name: "Adrian",
        image: require('../../../assets/user4.png')
      },
      {
        name: "Enrique",
        image: require('../../../assets/user5.png')
      }
    ],
    size: { width, height },
    matches: [
      {
        team1: "Sport Zeit",
        team2: "Telefónica FC",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "San Isidro",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer1.jpg')
      },
      {
        team1: "Barcelona",
        team2: "Claro",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "La Molina",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer2.jpg')
      },
      {
        team1: "Entel",
        team2: "Sacramento",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "Barranco",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer3.jpg')
      }
    ]
  }

  renderCarrouselPages() {
    return this.state.matches.map(match => (
      <CarouselPageComponent
        key={Math.random()}
        content={match}
      />
    ));
  }

  _onLayoutDidChange = e => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: layout.height } });
  };

  render() {

    return (
      <ScrollView style={{ flex: 1 }}>
        <View
          style={{ height: 180 }}
          onLayout={this._onLayoutDidChange}
        >
          <Carousel
            delay={2000}
            style={this.state.size}
            autoplay
            pageInfo
            bullets={true}
            currentPage={2}
          >
            {
              this.renderCarrouselPages()
            }
          </Carousel>
        </View>
        <View style={{ backgroundColor: '#fff' }}>
          <View style={{ padding: 12 }}>
            <Text style={{ color: "#000", fontWeight: 'bold' }}>Partidos para hoy</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ flex: 1 }}>Mar, 5 mar 5:00 - 10:00 PM</Text>
              <Text style={{ flex: 1, color: '#e87e44', textAlign: 'right' }}>Ver todos</Text>
            </View>
          </View>
          <TeamCardsListComponent
            matches={this.state.matchesCard}
            navigation={this.props.navigation}
          />
          <View style={{ padding: 12 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ flex: 2, fontWeight: 'bold', color: '#000' }}>Invitalos a jugar a tu equipo</Text>
              <Text style={{ flex: 1, color: '#e87e44', textAlign: 'right' }}>Ver todos</Text>
            </View>
          </View>
          <PlayersListComponent players={this.state.players} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
});