import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

export default class TeamCarouselComponent extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={this.props.image} />
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: { color: '#fff', fontSize: 14 },
  image: { width: 60 }
});