import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView
} from 'react-native';
import PlayerCardComponent from '../PlayerCard';

export default class PlayersListComponent extends Component {

  renderPlayers() {
    return this.props.players.map(player => (
      <PlayerCardComponent key={Math.random()} player={player} />
    ));
  }

  render() {

    return (
      <ScrollView
        contentContainerStyle={styles.scroll}
        horizontal={true}
      >
        {
          this.props.players && this.renderPlayers()
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scroll: { paddingHorizontal: 10 }
});