import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground
} from 'react-native';
import CarouselInformationComponent from '../CarouselInformation';
import TeamCarouselComponent from '../TeamCarousel';

const { width } = Dimensions.get('window');

export default class CarouselPageComponent extends Component {

  state = {
    size: { width, height: 180 }
  }

  render() {
    return (
      <View>
        <ImageBackground
          style={this.state.size}
          source={this.props.content.backgroundImage}
        >
        </ImageBackground>
        <View style={styles.overlay}>
          <View style={styles.contentContainer}>
            <TeamCarouselComponent
              image={this.props.content.image1}
              title={this.props.content.team1}
            />
            <CarouselInformationComponent
              title={this.props.content.place}
              subtitle={this.props.content.time}
            />
            <TeamCarouselComponent
              image={this.props.content.image2}
              title={this.props.content.team2}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'row'
  }
});