import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text
} from 'react-native';
import { withNavigation } from 'react-navigation';

class CardTeamComponent extends Component {

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => this.props.navigation.navigate('MatchDetails')}
        >
          <View style={styles.headContentContainer}>
            <Image
              style={styles.headImage}
              source={this.props.content.image1}
            />
            <Image
              style={styles.headImage}
              source={require('../../../assets/vs.png')}
            />
            <Image
              style={styles.headImage}
              source={this.props.content.image2}
            />
          </View>
          <View style={styles.bottomContainer}>
            <Text style={styles.title}>
              {this.props.content.title}
            </Text>
            <TouchableOpacity>
              <Text style={styles.buttonText}>UNETE</Text>
            </TouchableOpacity>
            <View style={styles.infoContainer}>
              <View style={styles.leftInfoContainer}>
                <Image
                  style={styles.leftIcon}
                  source={require('../../../assets/field.png')}
                />
                <Text>{this.props.content.place}</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Image
                  style={styles.rightIcon}
                  source={require('../../../assets/clock.png')}
                />
                <Text>{this.props.content.time}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(CardTeamComponent);

const styles = StyleSheet.create({
  container: {
    padding: 12,
    width: 270,
    elevation: 2,
    backgroundColor: '#fff'
  },
  rightIcon: { width: 17, height: 17, marginHorizontal: 6 },
  leftIcon: { width: 17, height: 17, marginHorizontal: 6 },
  leftInfoContainer: { flexDirection: 'row', marginRight: 10 },
  infoContainer: { flexDirection: 'row', paddingTop: 6 },
  buttonText: { color: '#e87e44' },
  headImage: {
    width: 60,
    marginHorizontal: 10
  },
  bottomContainer: {
    paddingVertical: 12,
    paddingHorizontal: 12,
    backgroundColor: '#fff',
    elevation: 2,
    borderBottomRightRadius: 18,
    borderBottomLeftRadius: 18
  },
  title: { fontWeight: 'bold' },
  headContentContainer: {
    elevation: 2,
    backgroundColor: '#2e2d2c',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 18,
    borderTopLeftRadius: 18,
    paddingTop: 5
  }
});