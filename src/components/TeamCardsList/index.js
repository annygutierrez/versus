import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView
} from 'react-native';
import CardTeamComponent from '../CardTeam';

export default class TeamCardsListComponent extends Component {

  renderCards() {
    return this.props.matches.map(match => (
      <CardTeamComponent
        key={Math.random()}
        content={match}
      />
    ));
  }

  render() {

    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        style={styles.scroll}
        contentContainerStyle={styles.contentScroll}
      >
        {
          this.props.matches && this.renderCards()
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scroll: { flexGrow: 1 },
  contentScroll: { backgroundColor: '#fff' }
});