import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

export default class PlayerCardComponent extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={this.props.player.image}
        />
        <Text>{this.props.player.name}</Text>
        <Text style={styles.buttonText}>INVITAR</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center', 
    marginHorizontal: 15
  },
  buttonText: { color: '#e87e44' },
  image: { width: 60, borderRadius: 10 }
});