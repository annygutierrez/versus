import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Image
} from 'react-native';

export default class MainHeaderComponent extends Component {

  render() {

    return (
      <ImageBackground
        source={require('../../../assets/fondo-top.png')}
        style={styles.container}
      >
        <Image
          style={styles.logo}
          source={require('../../../assets/logo.png')}
        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000'
  },
  logo: { width: 80, height: 20 }
});