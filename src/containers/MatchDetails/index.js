import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Image,
  FlatList,
  Modal,
  TouchableOpacity,
  Text,
  ScrollView,
  Dimensions,
  View
} from 'react-native';
import MainHeaderComponent from '../../components/MainHeader';
import Carousel from 'react-native-looped-carousel';
import CarouselPageComponent from '../../components/CarouselPage';

const { width, height } = Dimensions.get('window');

export default class MatchDetailsContainer extends Component {

  state = {
    showModal: false,
    matchesCard: [
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Zeit Sports vs. La Máquina Telefónica - 8 vs. 8",
        place: "San Isidro",
        time: "5:50 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Barcelona vs. Claro - 10 vs. 10",
        place: "La Molina",
        time: "1:30 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "The 1975 vs. Barranquilla - 8 vs. 8",
        place: "Barranco",
        time: "3:00 PM"
      },
      {
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        title: "Entel vs. Sacramento - 11 vs. 11",
        place: "Cieneguilla",
        time: "2:00 PM"
      }
    ],
    players: [
      {
        name: "Paolo",
        image: require('../../../assets/user1.png')
      },
      {
        name: "Leopardi",
        image: require('../../../assets/user2.png')
      },
      {
        name: "Camilo",
        image: require('../../../assets/user3.png')
      },
      {
        name: "Adrian",
        image: require('../../../assets/user4.png')
      },
      {
        name: "Enrique",
        image: require('../../../assets/user5.png')
      }
    ],
    size: { width, height },
    matches: [
      {
        team1: "Sport Zeit",
        team2: "Telefónica FC",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "San Isidro",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer1.jpg')
      },
      {
        team1: "Barcelona",
        team2: "Claro",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "La Molina",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer2.jpg')
      },
      {
        team1: "Entel",
        team2: "Sacramento",
        image1: require('../../../assets/squad1.png'),
        image2: require('../../../assets/squad2.png'),
        place: "Barranco",
        time: "Lun, 4 mar - 3:00 pm",
        backgroundImage: require('../../../assets/soccer3.jpg')
      }
    ]
  }

  players = [
    {
      name: 'Cristian'
    },
    {
      name: 'Cristian'
    },
    {
      name: 'Cristian'
    },
    {
      name: 'Cristian'
    },
    {
      name: 'Cristian'
    }
  ]

  renderItem(players) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 10 }}>
        <Image style={{ width: 50, height: 50 }} source={require('../../../assets/userCircle.png')} />
        <Text style={{ marginLeft: 10, color: '#000' }}>{players.name}</Text>
      </View>
    );
  }

  toggleModal(boolVal) {
    this.setState({ showModal: boolVal })
  }

  renderCarrouselPages() {
    return this.state.matches.map(match => (
      <CarouselPageComponent
        key={Math.random()}
        content={match}
      />
    ));
  }

  _onLayoutDidChange = e => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: layout.height } });
  };

  render() {

    return (
      <View style={styles.container}>
        <MainHeaderComponent />
        <View style={{ flex: 8 }}>
          <View
            style={{ height: 180, flex: 4 }}
            onLayout={this._onLayoutDidChange}
          >
            <Carousel
              delay={2000}
              style={this.state.size}
              autoplay
              pageInfo
              bullets={true}
              currentPage={2}
            >
              {
                this.renderCarrouselPages()
              }
            </Carousel>
          </View>
          <View style={{ paddingHorizontal: 24, paddingVertical: 18, flex: 1, backgroundColor: '#fff' }}>
            <Text style={{ color: "#000", fontWeight: 'bold' }}>
              Zeit Sports vs. La Maquina de Telefonica - 5 vs. 5
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 8 }}>
              <Image
                style={{ width: 17, height: 17, marginRight: 6 }}
                source={require('../../../assets/field.png')}
              />
              <Text style={{ fontSize: 12 }}>Av. Los Ruiseñores 900, Santa Anita</Text>
              <TouchableOpacity>
                <Text style={{ marginLeft: 22, fontSize: 10, color: "#e87e44" }}>Ver mapa</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ backgroundColor: "#f9f7fd", flex: 6 }}>
            <ScrollView style={{ flexGrow: 1 }} contentContainerStyle={{ paddingVertical: 8 }}>
              <View style={{ paddingHorizontal: 12, paddingVertical: 8, backgroundColor: "#f9f7fd" }}>
                <View style={{ backgroundColor: "#fff", borderRadius: 13, elevation: 2, padding: 18 }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image style={{ width: 60 }} source={require('../../../assets/squad1.png')} />
                      <Text style={{ color: "#000", marginLeft: 10, fontWeight: 'bold' }}>Zeit Sports</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                      <TouchableOpacity onPress={() => this.toggleModal(true)} activeOpacity={0.7} style={{ backgroundColor: "#e87e44", alignSelf: 'flex-end', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 12 }}>
                        <Text style={{ color: "#fff", fontSize: 12 }}>UNIRME AL EQUIPO</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <Text style={{ color: '#000', fontSize: 15 }}>Equipo ( 4 de 5 )</Text>
                  <FlatList
                    style={{
                      flex: 1,
                      paddingHorizontal: 5,
                      paddingVertical: 15
                    }}
                    horizontal={false}
                    numColumns={2}
                    scrollEnabled={false}
                    data={this.players}
                    keyExtractor={(item, index) => index}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => this.renderItem(item)}
                  />
                </View>
              </View>
              <View style={{ paddingHorizontal: 12, paddingVertical: 8, backgroundColor: "#f9f7fd" }}>
                <View style={{ backgroundColor: "#fff", borderRadius: 13, elevation: 2, padding: 18 }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image style={{ width: 60 }} source={require('../../../assets/squad2.png')} />
                      <Text style={{ color: "#000", marginLeft: 10, fontWeight: 'bold' }}>Zeit Sports</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                      <TouchableOpacity onPress={() => this.toggleModal(true)} activeOpacity={0.7} style={{ backgroundColor: "#e87e44", alignSelf: 'flex-end', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 12 }}>
                        <Text style={{ color: "#fff", fontSize: 12 }}>UNIRME AL EQUIPO</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <Text style={{ color: '#000', fontSize: 15 }}>Equipo ( 4 de 5 )</Text>
                  <FlatList
                    style={{
                      flex: 1,
                      paddingHorizontal: 5,
                      paddingVertical: 15
                    }}
                    horizontal={false}
                    numColumns={2}
                    scrollEnabled={false}
                    data={this.players}
                    keyExtractor={(item, index) => index}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => this.renderItem(item)}
                  />
                </View>
              </View>
              <View style={{ paddingHorizontal: 12, paddingVertical: 8, backgroundColor: "#f9f7fd" }}>
                <View style={{ borderRadius: 13, elevation: 2, backgroundColor: '#fff' }}>
                  <Image style={{ width: width - 24, height: 200, borderTopLeftRadius: 13, borderTopRightRadius: 13 }} source={require('../../../assets/soccer.jpg')} />
                  <View style={{ backgroundColor: '#fff', padding: 16, borderBottomRightRadius: 13, borderBottomLeftRadius: 13 }}>
                    <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>Cancha sintética "San Isidro"</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 6 }}>
                      <Image
                        style={{ width: 17, height: 17, marginRight: 6 }}
                        source={require('../../../assets/field.png')}
                      />
                      <Text style={{ fontSize: 12, color: '#000', marginRight: 30 }}>Av. Los Ruiseñores 900, Santa Anita</Text>
                      <Text style={{ color: '#e87e44', textAlign: 'right', fontSize: 10 }}>Ver mapas</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 6 }}>
                      <Image
                        style={{ width: 17, height: 17, marginRight: 6 }}
                        source={require('../../../assets/field.png')}
                      />
                      <Text style={{ fontSize: 12, color: '#000', marginRight: 30 }}>951659109 - 978294100</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showModal}
          onRequestClose={() => this.toggleModal(false)}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.toggleModal(false)}
            style={styles.touchableModal}>
            <View style={{ backgroundColor: '#fff', borderWidth: 5, borderColor: '#e87e44', borderRadius: 16, justifyContent: 'center', alignItems: 'center', paddingTop: 50, paddingHorizontal: 30, paddingBottom: 30 }}>
              <Image style={{  }} source={require('../../../assets/offside.png')} />
              <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: '#000', marginTop: 20 }}>¡Oh Oh! - Necesitas estar registrado para unirte a un equipo</Text>
              <TouchableOpacity style={{ backgroundColor: '#e87e44', marginTop: 20, paddingHorizontal: 50, paddingVertical: 12, borderRadius: 30 }}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>REGISTRARME</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  touchableModal: { flex: 1, justifyContent: 'center', alignContent: 'center', backgroundColor: 'rgba(51, 51, 51, 0.6)', paddingHorizontal: 35 },
  tabbar: {
    height: 38
  },
  indicator: {
    backgroundColor: '#6daa86',
    height: 3
  },
  label: {
    color: '#6daa86',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tab: {
    backgroundColor: '#fff'
  }
});