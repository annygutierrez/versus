import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions
} from 'react-native';
import MainHeaderComponent from '../../components/MainHeader';
import SoccerTabComponent from '../../components/SoccerTab';
import VoleyTabComponent from '../../components/VoleyTab';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

export default class HomeContainer extends Component {

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Futbol' },
      { key: 'second', title: 'Voley' },
    ],
  };

  _renderLabel = ({ route }) => (
    <Text style={{ color: "#6daa86" }}>{route.title}</Text>
  );

  _renderScene = SceneMap({
    first: SoccerTabComponent,
    second: VoleyTabComponent,
  });

  render() {
    return (
      <View style={styles.container}>
        <MainHeaderComponent />
        <View style={{ flex: 8 }}>
          <TabView
            style={styles.tabbar}
            navigationState={this.state}
            renderScene={this._renderScene}
            onIndexChange={index => this.setState({ index })}
            renderTabBar={(props) =>
              <TabBar
                renderLabel={this._renderLabel}
                {...props}
                style={styles.tab}
                labelStyle={styles.label}
                indicatorStyle={styles.indicator}
              />
            }
            initialLayout={{ width: Dimensions.get('window').width, height: 0 }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  tabbar: {
    height: 38
  },
  indicator: {
    backgroundColor: '#6daa86',
    height: 3
  },
  label: {
    color: '#6daa86',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tab: {
    backgroundColor: '#fff'
  }
});